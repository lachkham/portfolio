FROM node:13

WORKDIR /usr/src/app

COPY package.json .

RUN npm install -g gatsby-cli

RUN npm install

COPY ./entry.sh /
COPY . .
RUN chmod +x /entry.sh

EXPOSE 8000
ENTRYPOINT ["/entry.sh"]





