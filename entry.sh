#!/bin/sh
set -e

export GATSBY_DIR="/"

# Decide what to do
echo $1
if  [ "$1" = "develop" ]
then
  rm -rf $GATSBY_DIR/public
  gatsby develop --host 0.0.0.0

elif  [ "$1" = "build" ]
then
  rm -rf $GATSBY_DIR/public
  gatsby build

elif  [ "$1" = "prod" ]
then
  rm -rf $GATSBY_DIR/public
  gatsby build
  gatsby serve --verbose --prefix-paths --port 8000 --host 0.0.0.0

else
  exec $@

fi
