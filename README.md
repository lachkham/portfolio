# 1. First Step clone the repository

```
git clone https://gitlab.com/lachkham/portfolio/
cd portfolio
```

# 2. Second Step Build Docker Image from Dockerfile

```
docker build . -t ahmedlachkhamportfolio:1.0.1
```

# 3. Third Step run the container in one of these following mode 
- ### Develop

```
docker run -it  -p 8000:8000 ahmedlachkhamportfolio:1.0.1  develop
```
- ### Build

```
docker run  -it  -p 8000:8000 ahmedlachkhamportfolio:1.0.1  build
```

- ### Production
```
docker run -it  -p 8000:8000 ahmedlachkhamportfolio:1.0.1  prod
```
- ### Run a linux command 
```
docker run -it  -p 8000:8000 ahmedlachkhamportfolio:1.0.1  <Your-command>
```
>example: `docker run -it  -p 8000:8000 ahmedlachkhamportfolio:1.0.1 ls -l`
