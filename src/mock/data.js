/* eslint-disable prettier/prettier */
import { nanoid } from 'nanoid'; 

// HEAD DATA
export const headData = {
  title: '', // e.g: 'Name | Developer'
  lang: '', // e.g: en, es, fr, jp
  description: '', // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  title: '',
  name: 'Ahmed Lachkham',
  subtitle: 'Software Development Engineering student,\n looking for an End of studies project internship ',
  cta: 'Know me more',
};

// ABOUT DATA
export const aboutData = {
  img: 'profile.jpg',
  paragraphOne: 'What would define me:  Fast learner, dynamic, autonomous, with a lot of determination, result oriented and able to make decisions, to adapt to new working environments and new cultures.',
  paragraphTwo: 'My ambition is to succeed an international career, to be an R&D engineer who always seeks to be at the heart of technological and scientific progress.',
  paragraphThree: 'They said "Only dead fish go with the flow". I am always ready to overcome and surpass the flow of new challenges.',
  resume: 'https://zety.com/mycv/lachkhamahmed-EN', // if no resume, the button will not show up
};

// PROJECTS DATA
export const projectsData = [
  {
    id: nanoid(),
    img: 'projectqatar.jpg',
    title: 'Qatar World Cup',
    info: 'Qatar 2022 World Cup management web application',
    info2: 'Spring Boot, Angular8, PostgresSQL',
    url: 'https://qatar-web.herokuapp.com/',
    repo: 'https://gitlab.com/lachkham/quatar', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'projectparking.jpg',
    title: 'Parking tunisie',
    info: 'Development and implementation of parking spots reservation system across multiple car park using Google maps',
    info2: 'Spring Boot, Angular8, PostgresSQL',
    url: 'https://parking-tunisia-app.herokuapp.com/',
    repo: 'https://gitlab.com/lachkham/parking', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'projectM.jpg',
    title: 'Mobile Development Project: Management of ISI student services',
    info: 'Android application that uses Process Maker as a backend to manage student services.',
    info2: 'Android, Java, Xml, Process Maker',
    url: '',
    repo: 'https://gitlab.com/lachkham/process-app', // if no repo, the button will not show up
  },
  
];

// CONTACT DATA
export const contactData = {
  cta: '',
  btn: '',
  email: 'ahmed.lachkham.1994@gmail.com',
};

// FOOTER DATA
export const footerData = {
  networks: [
   
    
    {
      id: nanoid(),
      name: 'linkedin',
      url: 'https://www.linkedin.com/in/ahmed-lachkham/',
    },
    {
      id: nanoid(),
      name: 'gitlab',
      url: 'https://gitlab.com/lachkham',
    },
  ],
};

// Github start/fork buttons
export const githubButtons = {
  isEnabled: false, // set to false to disable the GitHub stars/fork buttons
};
